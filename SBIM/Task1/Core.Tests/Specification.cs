using System;
using System.IO;
using Xunit.Abstractions;

namespace Core.Tests
{
    public class Specification
    {
        protected ITestOutputHelper Logger { get; }
        
        public Specification(ITestOutputHelper testOutputHelper)
        {
            Console.SetOut(new ConsoleOutputConverter(testOutputHelper));
            this.Logger = testOutputHelper;
        }
    }
}