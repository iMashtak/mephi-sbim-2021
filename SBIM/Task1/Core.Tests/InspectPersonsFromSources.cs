using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PluginsConnector;
using Xunit;
using Xunit.Abstractions;
using static Core.SourceProvider;

namespace Core.Tests
{
    public class InspectPersonsFromSources : Specification
    {
        [Theory]
        [MemberData(nameof(PersonsData))]
        public void ShortNameIsCorrect(Person p)
        {
            Assert.Equal(p.ShortName, $"{p.Surname} {p.Name[0]}.{p.Patronymic[0]}.");
        }

        [Theory]
        [MemberData(nameof(PersonsData))]
        public void GetAgeIsCorrect(Person p)
        {
            var now = DateTime.Now;
            Assert.Equal(p.GetAge(now), GetAge(p, now));
        }

        private static int GetAge(Person @this, DateTime current)
        {
            var a = (current.Year * 100 + current.Month) * 100 + current.Day;
            var b = (@this.BirthDate.Year * 100 + @this.BirthDate.Month) * 100 + @this.BirthDate.Day;
            return (a - b) / 10000;
        }

        public static IEnumerable<object[]> PersonsData()
        {
            return LoadSources().Select(person => new object[] {person});
        }

        public InspectPersonsFromSources(ITestOutputHelper testOutputHelper) : base(testOutputHelper) { }

        static InspectPersonsFromSources()
        {
            Directory.SetCurrentDirectory($"{Directory.GetCurrentDirectory()}/../../../../app");
        }
    }
}