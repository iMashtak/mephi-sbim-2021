﻿using System;
using Plugin.Trust;

namespace Plugin.Spy
{
    public class FakePerson : TrustPerson
    {
        public override string ShortName => $"{this.Surname} {this.Patronymic[0]}.{this.Name[0]}";

        public override int GetAge(DateTime current) =>
            base.GetAge(current) - 1;

        public FakePerson(string surname, string name, string patronymic, DateTime birthDate) : base(
            surname,
            name,
            patronymic,
            birthDate
        ) { }
    }
}