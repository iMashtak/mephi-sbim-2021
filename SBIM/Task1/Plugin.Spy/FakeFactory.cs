using System;
using System.Collections.Generic;
using PluginsConnector;

namespace Plugin.Spy
{
    public class FakeFactory : Factory
    {
        public override IEnumerable<Person> GetPersons() =>
          new List<Person>
          {
              new FakePerson("Неясный", "Ясень", "Сидорович", DateTime.Now),
              new FakePerson("Важный", "Пень", "Семёнович", new DateTime(1944, 3, 4))
          };
    }
}