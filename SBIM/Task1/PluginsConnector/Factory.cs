using System.Collections.Generic;

namespace PluginsConnector
{
    public abstract class Factory
    {
        public abstract IEnumerable<Person> GetPersons();
    }
}