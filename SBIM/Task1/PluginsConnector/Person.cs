using System;

namespace PluginsConnector
{
    public abstract class Person
    {
        public abstract string   Surname    { get; }
        public abstract string   Name       { get; }
        public abstract string   Patronymic { get; }
        public abstract DateTime BirthDate  { get; }

        public abstract string ShortName { get; }

        public abstract string FullName { get; }

        public abstract int GetAge(DateTime current);
    }
}