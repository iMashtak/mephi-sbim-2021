using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using PluginsConnector;

namespace Core
{
    public static class SourceProvider
    {
        public static IEnumerable<Person> LoadSources() =>
            new DirectoryInfo(Directory.GetCurrentDirectory())
                .GetFiles("*.dll")
                .Where(file => file.Name.StartsWith("Plugin."))
                .SelectMany(file => Assembly.LoadFrom(file.Name).GetTypes())
                .Where(type => type.IsSubclassOf(typeof(Factory)))
                .Select(type => Activator.CreateInstance(type) as Factory)
                .Where(factory => factory is not null)
                .SelectMany(factory => factory.GetPersons());
    }
}