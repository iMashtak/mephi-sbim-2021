echo "--- Очищаем папку с плагинами ---"
echo "$ rm -rf app"
rm -rf app
echo
echo "--- Публикуем в папку с плагинами корректный плагин ---"
echo "$ dotnet publish Plugin.Trust/Plugin.Trust.csproj -o app"
dotnet publish Plugin.Trust/Plugin.Trust.csproj -o app
echo
echo "--- Запускаем тесты ---"
echo "$ dotnet test Core.Tests/Core.Tests.csproj"
dotnet test Core.Tests/Core.Tests.csproj
echo "--- Успешно завершены все тесты ---"
echo
echo "--- Публикуем в папку с плагинами некорректный плагин ---"
echo "$ dotnet publish Plugin.Spy/Plugin.Spy.csproj -o app"
dotnet publish Plugin.Spy/Plugin.Spy.csproj -o app
echo "--- Запускаем тесты ---"
echo "$ dotnet test Core.Tests/Core.Tests.csproj"
dotnet test Core.Tests/Core.Tests.csproj
echo "--- Тесты завершились с ошибкой ---"
