﻿using System;
using PluginsConnector;

namespace Plugin.Trust
{
    public class TrustPerson : Person
    {
        public override string   Surname    { get; }
        public override string   Name       { get; }
        public override string   Patronymic { get; }
        public override DateTime BirthDate  { get; }

        public override string ShortName => $"{this.Surname} {this.Name[0]}.{this.Patronymic[0]}.";

        public override string FullName => $"{this.Surname} {this.Name} {this.Patronymic}";

        public override int GetAge(DateTime current)
        {
            var a = (current.Year * 100 + current.Month) * 100 + current.Day;
            var b = (this.BirthDate.Year * 100 + this.BirthDate.Month) * 100 + this.BirthDate.Day;
            return (a - b) / 10000;
        }
        
        public TrustPerson(string surname, string name, string patronymic, DateTime birthDate)
        {
            this.Surname = surname;
            this.Name = name;
            this.Patronymic = patronymic;
            this.BirthDate = birthDate;
        }
    }
}