using System;
using System.Collections.Generic;
using PluginsConnector;

namespace Plugin.Trust
{
    public class TrustFactory : Factory
    {
        public override IEnumerable<Person> GetPersons() =>
            new List<Person>
            {
                new TrustPerson("Иванов", "Иван", "Иванович", new DateTime(1984, 10, 12)),
                new TrustPerson("Петров", "Пётр", "Петрович", new DateTime(2002, 10, 12))
            };
    }
}