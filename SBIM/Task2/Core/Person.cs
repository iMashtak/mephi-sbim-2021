﻿using System;

namespace Core
{
    public record Person(string Surname, string Name, string Patronymic, DateTime BirthDate)
    {
        public string ShortName => $"{this.Surname} {this.Name[0]}.{this.Patronymic[0]}.";

        public string FullName => $"{this.Surname} {this.Name} {this.Patronymic}";

        public int GetAge(DateTime current)
        {
            var a = (current.Year * 100 + current.Month) * 100 + current.Day;
            var b = (this.BirthDate.Year * 100 + this.BirthDate.Month) * 100 + this.BirthDate.Day;
            return (a - b) / 10000;
        }
    }
}