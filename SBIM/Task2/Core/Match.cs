using System;
using System.Collections.Generic;
using System.Linq;
using LanguageExt;
using LanguageExt.Common;

namespace Core
{
    public static class ErrorCode
    {
        public const int AlreadyRegistered = 0;
        public const int NotFound          = 1;
        public const int NotRecognized     = 2;
    }

    public class Evolvent
    {
        private static Dictionary<string, Evolvent> _evolvents = new();

        public static Either<Error, Evolvent> Get(string name)
        {
            if (_evolvents.ContainsKey(name)) { return _evolvents[name]; }
            return Error.New(ErrorCode.NotFound, "");
        }

        public Dictionary<string, Func<object, object>> Updates { get; }
        public Dictionary<string, object>               Creates { get; }
        public List<string>                             Removes { get; }

        private Evolvent(
            string                                    name,
            Dictionary<string, Func<object, object>>? updates = null,
            Dictionary<string, object>?               creates = null,
            List<string>?                             removes = null
        )
        {
            this.Updates = updates ?? new Dictionary<string, Func<object, object>>();
            this.Creates = creates ?? new Dictionary<string, object>();
            this.Removes = removes ?? new List<string>();
            _evolvents.Add(name, this);
        }

        public static Either<Error, Evolvent> New(
            string                                    name,
            Dictionary<string, Func<object, object>>? updates = null,
            Dictionary<string, object>?               creates = null,
            List<string>?                             removes = null
        )
        {
            return new Evolvent(name, updates, creates, removes);
        }
    }

    public class State
    {
        public  DateTime                   DateTime { get; }
        private Dictionary<string, object> Objects  { get; }

        public State(DateTime dateTime, Dictionary<string, object> objects)
        {
            this.DateTime = dateTime;
            this.Objects = objects;
        }

        public Either<Error, object> GetObject(string name)
        {
            return this.Objects.ContainsKey(name)
                ? this.Objects[name]
                : Error.New(ErrorCode.NotFound, "");
        }

        public State Evolve(DateTime dateTime, Evolvent evolvent)
        {
            Dictionary<string, object> objects = new();
            foreach (var (key, value) in this.Objects)
            {
                if (evolvent.Updates.ContainsKey(key))
                {
                    var update = evolvent.Updates[key];
                    objects.Add(key, update(value));
                    continue;
                }
                if (evolvent.Creates.ContainsKey(key))
                {
                    objects.Add(key, evolvent.Creates[key]);
                    continue;
                }
                if (evolvent.Removes.Contains(key))
                {
                    objects.Remove(key);
                    continue;
                }
                objects[key] = value;
            }
            return new State(dateTime, objects);
        }

        public override bool Equals(object obj) =>
            obj is State state && this.DateTime == state.DateTime;

        public override int GetHashCode() =>
            this.DateTime.GetHashCode();
    }

    public class Description
    {
        private static Dictionary<string, Description> _descriptions = new Dictionary<string, Description>();

        public static Either<Error, Description> New(
            string                                                name,
            Func<object, Dictionary<string, object>, State, bool> recognizePredicate
        )
        {
            return New(name, new Dictionary<string, object>(), recognizePredicate);
        }

        public static Either<Error, Description> New(
            string                                                name,
            Dictionary<string, object>                            context,
            Func<object, Dictionary<string, object>, State, bool> recognizePredicate
        )
        {
            Description description = new(name, context, recognizePredicate);
            if (_descriptions.ContainsKey(name))
            {
                return Error.New(ErrorCode.AlreadyRegistered, $"Description '{name}' have already registered");
            }
            _descriptions.Add(name, description);
            return description;
        }

        public static Either<Error, Description> Get(string name)
        {
            if (_descriptions.ContainsKey(name)) { return _descriptions[name]; }
            return Error.New(ErrorCode.NotFound, $"Description '{name}' not registered");
        }

        private Description(
            string                                                name,
            Dictionary<string, object>                            context,
            Func<object, Dictionary<string, object>, State, bool> recognizePredicate
        )
        {
            this.Name = name;
            this._context = context;
            this._recognizePredicate = recognizePredicate;
        }

        public string Name { get; }

        private Dictionary<string, object> _context;

        private Func<object, Dictionary<string, object>, State, bool> _recognizePredicate;

        public Either<Error, RecognizeResult> Recognize(object @object, State state)
        {
            if (this._recognizePredicate(@object, this._context, state))
            {
                return new RecognizeResult(@object, this, state);
            }
            return Error.New(ErrorCode.NotRecognized, "");
        }

        public Either<Error, Description> With(
            string name,
            Func<object, Dictionary<string, object>, State, Func<object, Dictionary<string, object>, State, bool>,
                    bool>
                recognizePredicate
        )
        {
            return this.With(name, new Dictionary<string, object>(), recognizePredicate);
        }

        public Either<Error, Description> With(
            string                     name,
            Dictionary<string, object> context,
            Func<object, Dictionary<string, object>, State, Func<object, Dictionary<string, object>, State, bool>,
                    bool>
                recognizePredicate
        )
        {
            return New(
                name,
                context.Union(this._context).ToDictionary(p => p.Key, p => p.Value),
                (@object, ctx, state) => recognizePredicate(@object, ctx, state, this._recognizePredicate)
            );
        }
    }

    public record RecognizeResult(object Object, Description Description, State State);
}