using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Xunit;

namespace Situations
{
    public class Second
    {
        [Fact]
        public void Exec()
        {
            Setup.Make();
            var student1 = new Person("Иванов", "Иван", "Иванович", new DateTime(1998, 10, 21));
            var student2 = new Person("Петров", "Иван", "Иванович", new DateTime(1998, 10, 21));
            var isStudent = Description.Get(Names.DescriptionIsStudent).First().Right;
            var isNotStudent = Description.Get(Names.DescriptionIsNotStudent).First().Right;
            var personsEqualToPersonB = Description.Get(Names.DescriptionPersonEqualToPersonB).First().Right;
            var nothing = Evolvent.Get(Names.EvolventNothing).First().Right;
            var surnameChanges = Evolvent.Get(Names.EvolventPersonAChangesSurnameToPetrov).First().Right;
            
            // Первое состояние - было два студента: Иванов и Петров
            var state1 = new State(
                DateTime.Now,
                new Dictionary<string, object>
                {
                    {Names.ObjectPersonA, student1},
                    {Names.ObjectPersonB, student2}
                }
            );
            Assert.True(isStudent.Recognize(state1.GetObject(Names.ObjectPersonA).First().Right, state1).IsRight);

            // Второе состояние - прошло 25 лет и первый из них перестал быть студентом
            var state2 = state1.Evolve(DateTime.Now.AddYears(25), nothing);
            Assert.True(isNotStudent.Recognize(state2.GetObject(Names.ObjectPersonA).First().Right, state2).IsRight);

            // Третье состояние - через год Иванов сменил фамилию на Петров и стал неотличим от Петрова
            var state3 = state2.Evolve(DateTime.Now.AddYears(26), surnameChanges);
            Assert.True(personsEqualToPersonB.Recognize(state3.GetObject(Names.ObjectPersonA).First().Right, state3).IsRight);
        }
    }
}