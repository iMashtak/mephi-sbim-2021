using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Xunit;

namespace Situations
{
    public class First
    {
        [Fact]
        public void Exec()
        {
            Setup.Make();
            var student1 = new Person("Иванов", "Иван", "Иванович", new DateTime(1998, 10, 21));
            var isStudent = Description.Get(Names.DescriptionIsStudent).First().Right;
            var isNotStudent = Description.Get(Names.DescriptionIsNotStudent).First().Right;
            var loseBitrhDate = Evolvent.Get(Names.EvolventPersonALosesBirthDate).First().Right;
            var restoreBirthDate = Evolvent.Get(Names.EvolventPersonARestoresBirthDate).First().Right;

            // Первое состояние - был один студент Иванов
            var state1 = new State(
                DateTime.Now,
                new Dictionary<string, object>
                {
                    {Names.ObjectPersonA, student1}
                }
            );
            Assert.True(isStudent.Recognize(state1.GetObject(Names.ObjectPersonA).First().Right, state1).IsRight);

            // Второе состояние - через день происходит ошибка в документах: Иванов теперь 1800-го года рождения, а значит не студент
            var state2 = state1.Evolve(DateTime.Now.AddDays(1), loseBitrhDate);
            Assert.True(isNotStudent.Recognize(state2.GetObject(Names.ObjectPersonA).First().Right, state2).IsRight);
            
            // Третье состояние - на следующий день Иванову восстанавливают дату рождения и он снова становится студентом
            var state3 = state2.Evolve(DateTime.Now.AddDays(2), restoreBirthDate);
            Assert.True(isStudent.Recognize(state3.GetObject(Names.ObjectPersonA).First().Right, state3).IsRight);
        }
    }
}