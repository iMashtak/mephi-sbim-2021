using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using Xunit;

namespace Situations
{
    public static class Names
    {
        public const string DescriptionIsStudent                  = nameof(DescriptionIsStudent);
        public const string DescriptionIsNotStudent               = nameof(DescriptionIsNotStudent);
        public const string DescriptionPersonEqualToPersonB       = nameof(DescriptionPersonEqualToPersonB);
        public const string ObjectPersonA                         = nameof(ObjectPersonA);
        public const string ObjectPersonB                         = nameof(ObjectPersonB);
        public const string EvolventNothing                       = nameof(EvolventNothing);
        public const string EvolventPersonALosesBirthDate         = nameof(EvolventPersonALosesBirthDate);
        public const string EvolventPersonARestoresBirthDate      = nameof(EvolventPersonARestoresBirthDate);
        public const string EvolventPersonAChangesSurnameToPetrov = nameof(EvolventPersonAChangesSurnameToPetrov);

        public const string ContextOtherPerson = nameof(ContextOtherPerson);
    }

    public class Setup
    {
        public static void Make()
        {
            var isStudentEither = Description.New(
                Names.DescriptionIsStudent,
                (@object, context, state) =>
                {
                    if (@object is not Person person) { return false; }
                    return person.GetAge(state.DateTime) is >= 15 and <= 25;
                }
            );
            Assert.True(isStudentEither.IsRight);
            var isStudent = isStudentEither.First().Right;
            var isNotStudentEither = isStudent.With(
                Names.DescriptionIsNotStudent,
                (@object, context, state, next) => !next(@object, context, state)
            );
            Assert.True(isNotStudentEither.IsRight);
            var personsEqual = Description.New(
                Names.DescriptionPersonEqualToPersonB,
                new Dictionary<string, object>
                {
                    {Names.ContextOtherPerson, Names.ObjectPersonB}
                },
                (@object, context, state) =>
                {
                    if (@object is not Person person) { return false; }
                    var otherId = context[Names.ContextOtherPerson] as string;
                    var other = state.GetObject(otherId);
                    if (other.IsLeft || other.First().Right is not Person otherPerson) { return false; }
                    return person.Equals(otherPerson);
                }
            );
            Assert.True(personsEqual.IsRight);

            var e0 = Evolvent.New(Names.EvolventNothing);
            Assert.True(e0.IsRight);
            var e1 = Evolvent.New(
                Names.EvolventPersonALosesBirthDate,
                updates: new Dictionary<string, Func<object, object>>
                {
                    {
                        Names.ObjectPersonA, student => (Person) student with
                        {
                            BirthDate = new DateTime(1800, 1, 1)
                        }
                    }
                }
            );
            Assert.True(e1.IsRight);
            var e2 = Evolvent.New(
                Names.EvolventPersonARestoresBirthDate,
                updates: new Dictionary<string, Func<object, object>>
                {
                    {
                        Names.ObjectPersonA, student => (Person) student with
                        {
                            BirthDate = new DateTime(1998, 10, 21)
                        }
                    }
                }
            );
            Assert.True(e2.IsRight);
            var e3 = Evolvent.New(
                Names.EvolventPersonAChangesSurnameToPetrov,
                updates: new Dictionary<string, Func<object, object>>
                {
                    {
                        Names.ObjectPersonA, student => (Person) student with
                        {
                            Surname = "Петров"
                        }
                    }
                }
            );
            Assert.True(e3.IsRight);
        }
    }
}