using System;
using System.Linq;
using Core.Relational;
using Core.WeakTyping;
using LanguageExt.Common;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace Simulation
{
    public class Situation : Specification
    {
        public Situation(ITestOutputHelper testOutputHelper) : base(testOutputHelper) { }

        [Fact]
        public async void SimulateOnRelational()
        {
            // Arrange
            this.FillDatabase();
            await using var context = RelationalDbContext.New();

            // Act
            var student = (await Student.Get("Иван", "Иванов", ctx: context)).First();
            var teacher = (await Teacher.Create(student.Name, student.Surname, "", context)).First().Right;
            var discipline = (await Discipline.Get("Параллельное программирование", ctx: context)).First();
            teacher.Teach.Add(discipline);
            await context.SaveChangesAsync();

            // Assert
            var studentTeachesHimself = (await Educate.Inspect(
                student.Name,
                student.Surname,
                student.Name,
                student.Surname,
                discipline.Title,
                context
            )).First().Right;
            Assert.True(studentTeachesHimself);
        }

        [Fact]
        public void SimulateOnWeakTyping()
        {
            SystemLog.Enable();
            InspectionSystem.UsePersonTeachHimselfInspection();
            using var context = WeakTypingDbContext.New();
            var lang = new WeakTypingLang(context);
            Func<Error, WeakTypingLang> ex = e => throw new Exception($"Errors: {e.Code}, {e.Message}");

            // Fill Database
            lang.Attribute("Name").Create(p => true).IfLeft(ex);
            lang.Attribute("Title").Create(p => true).IfLeft(ex);
            lang.Attribute("Surname").Create(p => true).IfLeft(ex);
            lang.Attribute("Semester").Create(p => (int)p.Value.Object <= 8).IfLeft(ex);
            lang.Attribute("AdmissionYear").Create(p => (int)p.Value.Object > 1970).IfLeft(ex);
            lang.Attribute("EmploymentHistory").Create(p => true).IfLeft(ex);
            lang.Concept("Student").Create("Name", "Surname", "AdmissionYear").IfLeft(ex);
            lang.Concept("Teacher").Create("Name", "Surname", "EmploymentHistory").IfLeft(ex);
            lang.Concept("Discipline").Create("Title", "Semester").IfLeft(ex);
            lang.Concept("Teach").Create("Concept::Discipline", "Concept::Teacher").IfLeft(ex);
            lang.Concept("Study").Create("Concept::Discipline", "Concept::Student").IfLeft(ex);
            lang.Value(1).Create(2, "Semester").IfLeft(ex);
            lang.Value(2).Create(5, "Semester").IfLeft(ex);
            lang.Value(3).Create(2016, "AdmissionYear").IfLeft(ex);
            lang.Value(4).Create("Иванов", "Surname").IfLeft(ex);
            lang.Value(5).Create("Иван", "Name").IfLeft(ex);
            lang.Value(6).Create("Параллельное программирование", "Title").IfLeft(ex);
            lang.Individual(1).Create(3, 4, 5).IfLeft(ex);
            lang.Individual(2).Create(6, 2).IfLeft(ex);

            // We have student and discipline
            Assert.True(lang.Individual(1).BelongsTo("Student"));
            Assert.False(lang.Individual(1).BelongsTo("Teacher"));
            Assert.True(lang.Individual(2).BelongsTo("Discipline"));

            // We state that student studies discipline
            lang.Value(7).Create(lang.Individual(1).Get().First(), "Concept::Student").IfLeft(ex);
            lang.Value(8).Create(lang.Individual(2).Get().First(), "Concept::Discipline").IfLeft(ex);
            lang.Individual(3).Create(7, 8).IfLeft(ex);
            Assert.True(lang.Individual(3).BelongsTo("Study"));

            // We update student to be also a teacher
            lang.Value(9).Create("Преподаватель кафедры 101 'НИУ КФС'", "EmploymentHistory").IfLeft(ex);
            lang.Individual(1).AddValues(9).IfLeft(e => throw new Exception($"Errors: {e.Code}, {e.Message}"));
            lang.Value(10).Create(lang.Individual(1).Get().First(), "Concept::Teacher").IfLeft(ex);
            Assert.True(lang.Individual(1).BelongsTo("Teacher"));
            
            lang.Concept("Educate").Create(
                arg => Recognizers.EducateRecognize(arg.Lang, arg.ConceptName, arg.IndividualId)
            );
            
            // We create virus individual - there ind1 both student and teacher (vals 7 and 10)
            lang.Individual(4).Create(7, 8, 10).IfLeft(ex);
            Assert.True(lang.Individual(4).BelongsTo("Teach"));
            Assert.True(lang.Individual(4).BelongsTo("Educate"));
        }
    }

    public static class Recognizers
    {
        public static bool EducateRecognize(WeakTypingLang lang, string conceptName, int individualId)
        {
            var ind = lang.Individual(individualId).Get().First();
            var studentIndVal = ind.Values
                .FirstOrDefault(v => v.Object is Individual i && lang.Individual(i.Id).BelongsTo("Student"));
            var teacherIndVal = ind.Values
                .FirstOrDefault(v => v.Object is Individual i && lang.Individual(i.Id).BelongsTo("Teacher"));
            var disciplineIndVal = ind.Values
                .FirstOrDefault(v => v.Object is Individual i && lang.Individual(i.Id).BelongsTo("Discipline"));

            if (studentIndVal is null || teacherIndVal is null || disciplineIndVal is null) { return false; }

            var studyExists = lang.Context.Individuals.ToList().Exists(
                i => lang.Individual(i.Id).BelongsTo("Study") &&
                    i.Values.Any(v => v.Id == studentIndVal.Id || v.Id == disciplineIndVal.Id)
            );
            var teachExists = lang.Context.Individuals.ToList().Exists(
                i => lang.Individual(i.Id).BelongsTo("Teach") &&
                    i.Values.Any(v => v.Id == teacherIndVal.Id || v.Id == disciplineIndVal.Id)
            );

            return studyExists && teachExists;
        }
    }
}