using System;
using Core.Data;
using Core.Tests;
using Xunit.Abstractions;

namespace Simulation
{
    public class Specification
    {
        protected ITestOutputHelper Logger { get; }
        
        public Specification(ITestOutputHelper testOutputHelper)
        {
            Console.SetOut(new ConsoleOutputConverter(testOutputHelper));
            this.Logger = testOutputHelper;
        }

        protected void FillDatabase()
        {
            if (_imported) return;
            var importTask = Operations.Import("../../../../Core/Data");
            importTask.Wait();
            _imported = true;
        }

        private static bool _imported;
    }
}