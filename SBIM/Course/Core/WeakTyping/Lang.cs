using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LanguageExt;
using LanguageExt.Common;
using Microsoft.EntityFrameworkCore;

namespace Core.WeakTyping
{
    public class WeakTypingLang
    {
        public WeakTypingDbContext Context { get; }

        public ICollection<Individual> Individuals { get; }

        public WeakTypingLang(WeakTypingDbContext context)
        {
            this.Context = context;
            this.Individuals = new List<Individual>();
        }

        public static WeakTypingLang Init(WeakTypingDbContext context) =>
            new(context);

        public ConceptOperations Concept(string name) =>
            new(this, name);

        public static event Action<Concept> OnConceptCreation = _ => { };

        public record ConceptOperations(WeakTypingLang Lang, string Name)
        {
            public Either<Error, WeakTypingLang> Create(params string[] attributeNames)
            {
                return this.Create(_ => true, attributeNames);
            }

            public Either<Error, WeakTypingLang> Create(
                Expression<Func<(WeakTypingLang Lang, string ConceptName, int IndividualId), bool>> criteria,
                params string[]                                                                     attributeNames
            )
            {
                var attributes = new List<Attribute>();
                foreach (var attributeName in attributeNames)
                {
                    var attribute = this.Lang.Context.Attributes.FirstOrDefault(a => a.Name == attributeName);
                    if (attribute is null) { return Errors.NotFound(); }
                    attributes.Add(attribute);
                }
                return this.Create(criteria, attributes);
            }

            public static bool ConceptAttributeConstraint(WeakTypingLang lang, Value value, string conceptName)
            {
                return value.Object is Individual i &&
                    lang.Individual(i.Id).BelongsTo(conceptName);
            }

            public Either<Error, WeakTypingLang> Create(
                Expression<Func<(WeakTypingLang Lang, string ConceptName, int IndividualId), bool>> criteria,
                IEnumerable<Attribute>                                                              attributes
            )
            {
                var concept = new Concept(this.Name, criteria);
                foreach (var attribute in attributes)
                {
                    concept.Attributes.Add(attribute);
                }
                OnConceptCreation(concept);
                this.Lang.Context.Add(concept);
                try
                {
                    this.Lang.Context.SaveChanges();
                    var result = this.Lang
                        .Attribute($"Concept::{this.Name}")
                        .Create(args => ConceptAttributeConstraint(args.Lang, args.Value, concept.Name));
                    return result;
                }
                catch (DbUpdateException e)
                {
                    var error = Error.New($"Error while creating concept {this.Name}", e);
                    return error;
                }
            }

            public Option<Concept> Get()
            {
                var concept = this.Lang.Context
                    .Concepts.Where(c => c.Name == this.Name)
                    .Include(c => c.Attributes)
                    .FirstOrDefault();
                return concept ?? Option<Concept>.None;
            }
        }

        public AttributeOperations Attribute(string name) =>
            new(this, name);

        public record AttributeOperations(WeakTypingLang Lang, string Name)
        {
            public Either<Error, WeakTypingLang> Create(
                Expression<Func<(WeakTypingLang Lang, Value Value), bool>> constraint
            )
            {
                var attribute = new Attribute(this.Name, constraint);
                this.Lang.Context.Add(attribute);
                this.Lang.Context.SaveChanges();
                return this.Lang;
            }

            public Option<Attribute> Get()
            {
                return this.Lang.Context.Attributes.FirstOrDefault(a => a.Name == this.Name) ?? Option<Attribute>.None;
            }
        }

        public ValueOperations Value(int id) =>
            new(this, id);

        public record ValueOperations(WeakTypingLang Lang, int Id)
        {
            public Either<Error, WeakTypingLang> Create(object @object, string attributeName)
            {
                var attributeOption = this.Lang.Attribute(attributeName).Get();
                return attributeOption.IsNone
                    ? Errors.NotFound()
                    : this.Create(@object, attributeOption.First());
            }

            public Either<Error, WeakTypingLang> Create(object @object, Attribute attribute)
            {
                var value = new Value(this.Id, @object);
                var accepts = attribute.Constraint.Compile().Invoke((this.Lang, value));
                if (!accepts) { return Errors.UnacceptableValueForAttribute(); }
                value.Attributes.Add(attribute);
                this.Lang.Context.Add(value);
                this.Lang.Context.SaveChanges();
                return this.Lang;
            }

            public Option<Value> Get()
            {
                return this.Lang.Context.Values.FirstOrDefault(v => v.Id == this.Id) ?? Option<Value>.None;
            }
        }

        public IndividualOperations Individual(int id) =>
            new(this, id);

        public static event Action<Individual> OnIndividualCreate    = _ => { };
        public static event Action<Individual> OnIndividualGet       = _ => { };
        public static event Action<Individual> OnIndividualBelongsTo = _ => { };
        public static event Action<Individual> OnIndividualAddValues = _ => { };

        public record IndividualOperations(WeakTypingLang Lang, int Id)
        {
            public Either<Error, WeakTypingLang> Create(params int[] ids)
            {
                var list = new List<Value>();
                foreach (var id in ids)
                {
                    var valueOption = this.Lang.Value(id).Get();
                    if (valueOption.IsNone) { return Errors.NotFound(); }
                    list.Add(valueOption.First());
                }
                return this.Create(list);
            }

            public Either<Error, WeakTypingLang> Create(IEnumerable<Value> values)
            {
                var individual = new Individual(this.Id);
                foreach (var value in values)
                {
                    individual.Values.Add(value);
                }
                this.Lang.Context.Add(individual);
                OnIndividualCreate(individual);
                this.Lang.Context.SaveChanges();
                return this.Lang;
            }

            public Option<Individual> Get()
            {
                var i = this.Lang.Context.Individuals
                        .Include(i => i.Values)
                        .ThenInclude(v => v.Attributes)
                        .FirstOrDefault(v => v.Id == this.Id) ??
                    Option<Individual>.None;
                if (i.IsSome)
                {
                    OnIndividualGet(i.First());
                    return i;
                }
                i = this.Lang.Individuals.FirstOrDefault(v => v.Id == this.Id) ?? Option<Individual>.None;
                if (i.IsSome)
                {
                    OnIndividualGet(i.First());
                }
                return i;
            }

            public bool BelongsTo(string conceptName)
            {
                var conceptO = this.Lang.Concept(conceptName).Get();
                if (conceptO.IsNone) { return false; }
                var individualO = this.Lang.Individual(this.Id).Get();
                if (individualO.IsNone) { return false; }
                var concept = conceptO.First();
                var individual = individualO.First();
                OnIndividualBelongsTo(individual);
                var iAttrs = individual.Values.SelectMany(v => v.Attributes).ToList();
                return concept.Attributes.All(cAttr => iAttrs.Contains(cAttr)) &&
                    concept.Criteria.Compile().Invoke((this.Lang, conceptName, this.Id));
            }

            public Either<Error, Individual> AddValues(params int[] ids)
            {
                var list = new List<Value>();
                foreach (var id in ids)
                {
                    var valueOption = this.Lang.Value(id).Get();
                    if (valueOption.IsNone) { return Errors.NotFound(); }
                    list.Add(valueOption.First());
                }
                return this.AddValues(list);
            }

            public Either<Error, Individual> AddValues(IEnumerable<Value> values)
            {
                var individual = this.Lang.Individual(this.Id).Get().First();
                foreach (var value in values)
                {
                    individual.Values.Add(value);
                }
                OnIndividualAddValues(individual);
                this.Lang.Context.SaveChanges();
                return individual;
            }
        }
    }
}