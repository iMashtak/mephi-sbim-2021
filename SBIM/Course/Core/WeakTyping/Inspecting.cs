using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Core.WeakTyping
{
    public record SystemLogRecord(
        int      Id,
        DateTime Timestamp,
        string   Action
    )
    {
        [NotMapped] public Concept?    Concept    { get; set; }
        [NotMapped] public Attribute?  Attribute  { get; set; }
        [NotMapped] public Individual? Individual { get; set; }
        [NotMapped] public Value?      Value      { get; set; }
    }

    public static class SystemLog
    {
        public static event Action<SystemLogRecord> OnUpdate = _ => { };

        public static void Enable()
        {
            void F<T>(T individual, string op, Action<SystemLogRecord> action, bool update = false)
            {
                using var ctx = WeakTypingDbContext.New();
                var record = new SystemLogRecord(0, DateTime.Now, $"{op}:{nameof(T)}:{individual}");
                ctx.Add(record);
                ctx.SaveChanges();
                action(record);
                if (update) OnUpdate(record);
            }

            WeakTypingLang.OnIndividualCreate += individual =>
            {
                F(individual, "Create", record => record.Individual = individual, true);
            };
            WeakTypingLang.OnIndividualGet += individual =>
            {
                F(individual, "Get", record => record.Individual = individual);
            };
            WeakTypingLang.OnIndividualBelongsTo += individual =>
            {
                F(individual, "BelongsTo", record => record.Individual = individual);
            };
            WeakTypingLang.OnIndividualAddValues += individual =>
            {
                F(individual, "AddValues", record => record.Individual = individual);
            };
        }
    }

    public static class InspectionSystem
    {
        private static string _personTeachHimselfInspection =
            "One person cannot study some discipline and teach it at the same time";

        public static void UsePersonTeachHimselfInspection()
        {
            void Inspection(SystemLogRecord record)
            {
                if (record.Individual is null) { return; }
                var ind = record.Individual;
                using var ctx = WeakTypingDbContext.New();
                var lang = WeakTypingLang.Init(ctx);
                lang.Individuals.Add(ind);
                var educateOption = lang.Concept("Educate").Get();
                if (educateOption.IsNone) { return; }
                if (ind.BelongsTo(lang, educateOption.First())) return;
                var studentIndVal = ind.Values
                    .FirstOrDefault(v => v.Object is Individual i && lang.Individual(i.Id).BelongsTo("Student"));
                var teacherIndVal = ind.Values
                    .FirstOrDefault(v => v.Object is Individual i && lang.Individual(i.Id).BelongsTo("Teacher"));
                if (studentIndVal is null || teacherIndVal is null) { return; }
                if (studentIndVal.Object is not Individual s || teacherIndVal.Object is not Individual t) return;
                if (s.Id == t.Id)
                {
                    throw new InspectionException(
                        $"PersonTeachHimselfInspection: individual with Id='{s.Id}' violates inspection. Description: {_personTeachHimselfInspection}"
                    );
                }
            }

            SystemLog.OnUpdate += Inspection;
        }
    }

    public class InspectionException : Exception
    {
        public InspectionException(string message) : base(message) { }
    }
}