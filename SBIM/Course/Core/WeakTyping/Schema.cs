using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.Json.Serialization;

namespace Core.WeakTyping
{
    public record Concept(
        string                                                                              Name,
        Expression<Func<(WeakTypingLang Lang, string ConceptName, int IndividualId), bool>> Criteria
    )
    {
        [JsonIgnore] public ICollection<Attribute> Attributes { get; } = new List<Attribute>();
    }

    public record Attribute(
        string                                                     Name,
        Expression<Func<(WeakTypingLang Lang, Value Value), bool>> Constraint
    )
    {
        [JsonIgnore] public ICollection<Concept> Concepts { get; } = new List<Concept>();
        [JsonIgnore] public ICollection<Value>   Values   { get; } = new List<Value>();
    }

    public record Individual(int Id)
    {
        [JsonIgnore] public ICollection<Value> Values { get; } = new List<Value>();

        public bool BelongsTo(WeakTypingLang lang, Concept concept)
        {
            var iAttrs = this.Values.SelectMany(v => v.Attributes).ToList();
            return concept.Attributes.All(cAttr => iAttrs.Contains(cAttr)) &&
                concept.Criteria.Compile().Invoke((lang, concept.Name, this.Id));
        }
    }

    public record Value(int Id, object Object)
    {
        [JsonIgnore] public ICollection<Attribute>  Attributes  { get; } = new List<Attribute>();
        [JsonIgnore] public ICollection<Individual> Individuals { get; } = new List<Individual>();
    }
}