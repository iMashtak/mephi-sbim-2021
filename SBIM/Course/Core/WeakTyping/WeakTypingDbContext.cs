using System;
using Microsoft.EntityFrameworkCore;
using Serialize.Linq;
using Serialize.Linq.Extensions;
using Serialize.Linq.Factories;
using Serialize.Linq.Serializers;

namespace Core.WeakTyping
{
    public class WeakTypingDbContext : DbContext
    {
        public DbSet<Concept>         Concepts    { get; set; }
        public DbSet<Attribute>       Attributes  { get; set; }
        public DbSet<Individual>      Individuals { get; set; }
        public DbSet<Value>           Values      { get; set; }
        public DbSet<SystemLogRecord> SystemLog   { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Concept>(
                concept =>
                {
                    concept.HasKey(c => c.Name);
                    concept.Property(c => c.Criteria).HasConversion<string>(
                        expression => expression.ToJson(new FactorySettings()),
                        str =>
                            new ExpressionSerializer(new JsonSerializer(), null)
                                .DeserializeText(str)
                                .ToExpressionNode(new FactorySettings())
                                .ToExpression<Func<(WeakTypingLang Lang, string ConceptName, int IndividualId), bool>>(new ExpressionContext())
                    );
                    concept.HasMany(c => c.Attributes).WithMany(a => a.Concepts);
                }
            );
            modelBuilder.Entity<Attribute>(
                attribute =>
                {
                    attribute.HasKey(a => a.Name);
                    attribute.Property(a => a.Constraint).HasConversion<string>(
                        expression => expression.ToJson(new FactorySettings()),
                        str =>
                            new ExpressionSerializer(new JsonSerializer(), null)
                                .DeserializeText(str)
                                .ToExpressionNode(new FactorySettings())
                                .ToExpression<Func<(WeakTypingLang Lang, Value Value), bool>>(new ExpressionContext())
                    );
                }
            );
            modelBuilder.Entity<Value>(
                value =>
                {
                    value.Property(v => v.Object).HasConversion<string>(
                        @object => System.Text.Json.JsonSerializer.Serialize(@object, null),
                        str => System.Text.Json.JsonSerializer.Deserialize<object>(str, null)!
                    );
                    value.HasMany(v => v.Attributes).WithMany(a => a.Values);
                }
            );
            modelBuilder.Entity<Individual>(
                individual =>
                {
                    individual.HasMany(i => i.Values).WithMany(v => v.Individuals);
                }
            );
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder
                .UseNpgsql("Host=localhost;Port=5432;Database=sbim;Username=postgres;Password=passw0rd")
                .EnableDetailedErrors()
                .EnableSensitiveDataLogging();
        }

        private WeakTypingDbContext()
        {
            if (_wasCreated) return;
            this.Database.EnsureDeleted();
            this.Database.EnsureCreated();
            _wasCreated = true;
        }

        private static volatile bool _wasCreated;

        public static WeakTypingDbContext New()
        {
            var context = new WeakTypingDbContext();
            return context;
        }
    }
}