using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using LanguageExt;
using LanguageExt.Common;
using Microsoft.EntityFrameworkCore;

namespace Core.Relational
{
    public record Student(
        string Name,
        string Surname,
        int    AdmissionYear
    )
    {
        public ICollection<Discipline> Study { get; } = new List<Discipline>();

        public static async Task<Either<Error, Student>> Create(
            string               name,
            string               surname,
            int                  admissionYear,
            RelationalDbContext? ctx = null
        )
        {
            var student = new Student(name, surname, admissionYear);
            var context = ctx ?? RelationalDbContext.New();
            try
            {
                context.Add(student);
                await context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (ctx is null) { await context.DisposeAsync(); }
                return Error.New(e);
            }
            if (ctx is null) { await context.DisposeAsync(); }
            return student;
        }

        public static async Task<List<Student>> Get(
            string               name          = "",
            string               surname       = "",
            int                  admissionYear = 0,
            RelationalDbContext? ctx           = null
        )
        {
            var context = ctx ?? RelationalDbContext.New();
            var students = context.Students;
            List<Student> result;
            if (name != "" && surname != "")
            {
                result = await students.Where(s => s.Name == name && s.Surname == surname).ToListAsync();
            }
            else if (name != "")
            {
                result = await students.Where(s => s.Name == name).ToListAsync();
            }
            else if (surname != "")
            {
                result = await students.Where(s => s.Surname == surname).ToListAsync();
            }
            else if (admissionYear != 0)
            {
                result = await students.Where(s => s.AdmissionYear == admissionYear).ToListAsync();
            }
            else
            {
                result = await students.ToListAsync();
            }
            if (ctx is null) { await context.DisposeAsync(); }
            return result;
        }
    }

    public record Teacher(
        string Name,
        string Surname,
        string EmploymentHistory
    )
    {
        public ICollection<Discipline> Teach { get; } = new List<Discipline>();

        public static async Task<Either<Error, Teacher>> Create(
            string               name,
            string               surname,
            string               employmentHistory,
            RelationalDbContext? ctx = null
        )
        {
            var teacher = new Teacher(name, surname, employmentHistory);
            var context = ctx ?? RelationalDbContext.New();
            try
            {
                context.Add(teacher);
                await context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (ctx is null) { await context.DisposeAsync(); }
                return Error.New(e);
            }
            if (ctx is null) { await context.DisposeAsync(); }
            return teacher;
        }

        public static async Task<List<Teacher>> Get(
            string               name    = "",
            string               surname = "",
            RelationalDbContext? ctx     = null
        )
        {
            var context = ctx ?? RelationalDbContext.New();
            var teachers = context.Teachers;
            List<Teacher> result;
            if (name != "" && surname != "")
            {
                result = await teachers.Where(t => t.Name == name && t.Surname == surname).ToListAsync();
            }
            else
            {
                result = await teachers.ToListAsync();
            }
            if (ctx is null) { await context.DisposeAsync(); }
            return result;
        }
    }

    public record Discipline(
        string Title,
        int    Semester
    )
    {
        public ICollection<Student> Study { get; } = new List<Student>();
        public ICollection<Teacher> Teach { get; } = new List<Teacher>();

        public static async Task<Either<Error, Discipline>> Create(
            string               name,
            int                  semester,
            RelationalDbContext? ctx = null
        )
        {
            var discipline = new Discipline(name, semester);
            var context = ctx ?? RelationalDbContext.New();
            try
            {
                context.Add(discipline);
                await context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (ctx is null) { await context.DisposeAsync(); }
                return Error.New(e);
            }
            if (ctx is null) { await context.DisposeAsync(); }
            return discipline;
        }

        public static async Task<List<Discipline>> Get(
            string               name     = "",
            int                  semester = 0,
            RelationalDbContext? ctx      = null
        )
        {
            var context = ctx ?? RelationalDbContext.New();
            var disciplines = context.Disciplines;
            List<Discipline> result;
            if (name != "")
            {
                result = await disciplines.Where(d => d.Title == name).ToListAsync();
            }
            else if (semester != 0)
            {
                result = await disciplines.Where(d => d.Semester == semester).ToListAsync();
            }
            else
            {
                result = await disciplines.ToListAsync();
            }
            if (ctx is null) { await context.DisposeAsync(); }
            return result;
        }
    }
}