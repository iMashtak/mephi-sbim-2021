using Microsoft.EntityFrameworkCore;

namespace Core.Relational
{
    public class RelationalDbContext : DbContext
    {
        public DbSet<Student>    Students    { get; set; }
        public DbSet<Discipline> Disciplines { get; set; }
        public DbSet<Teacher>    Teachers    { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Student>(
                student =>
                {
                    student.HasKey(
                        s => new
                        {
                            s.Name,
                            s.Surname
                        }
                    );
                }
            );
            modelBuilder.Entity<Teacher>(
                teacher =>
                {
                    teacher.HasKey(
                        t => new
                        {
                            t.Name,
                            t.Surname
                        }
                    );
                }
            );
            modelBuilder.Entity<Discipline>(
                discipline =>
                {
                    discipline.HasKey(d => d.Title);
                    discipline.HasMany(d => d.Study).WithMany(s => s.Study);
                    discipline.HasMany(d => d.Teach).WithMany(t => t.Teach);
                }
            );
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder
                .UseNpgsql("Host=localhost;Port=5432;Database=sbim;Username=postgres;Password=passw0rd")
                .EnableDetailedErrors()
                .EnableSensitiveDataLogging();
        }

        private RelationalDbContext()
        {
            if (_wasCreated) return;
            this.Database.EnsureDeleted();
            this.Database.EnsureCreated();
            _wasCreated = true;
        }

        private static volatile bool _wasCreated;

        public static RelationalDbContext New()
        {
            var context = new RelationalDbContext();
            return context;
        }
    }
}