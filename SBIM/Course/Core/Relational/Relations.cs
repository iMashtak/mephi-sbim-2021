using System;
using System.Linq;
using System.Threading.Tasks;
using LanguageExt;
using LanguageExt.Common;
using Microsoft.EntityFrameworkCore;

namespace Core.Relational
{
    public class Study
    {
        public static async Task<Error> Emit(
            string           studentName,
            string           studentSurname,
            string           disciplineName,
            RelationalDbContext? ctx = null
        )
        {
            var context = ctx ?? RelationalDbContext.New();
            var disciplines = await Discipline.Get(name: disciplineName, ctx: context);
            if (disciplines.Count > 1) { throw new NotSupportedException(); }
            var discipline = disciplines[0];
            var students = await Student.Get(name: studentName, surname: studentSurname, ctx: context);
            if (students.Count > 1) { throw new NotImplementedException(); }
            var student = students[0];
            student.Study.Add(discipline);
            await context.SaveChangesAsync();
            if (ctx is null) { await context.DisposeAsync(); }
            return Error.Bottom;
        }
    }

    public class Teach
    {
        public static async Task<Error> Emit(
            string           teacherName,
            string           teacherSurname,
            string           disciplineName,
            RelationalDbContext? ctx = null
        )
        {
            var context = ctx ?? RelationalDbContext.New();
            var disciplines = await Discipline.Get(name: disciplineName, ctx: context);
            if (disciplines.Count != 1) { return Errors.NotFound(); }
            var discipline = disciplines.First();
            var teachers = await Teacher.Get(name: teacherName, surname: teacherSurname, ctx: context);
            if (teachers.Count != 1) { return Errors.NotFound(); }
            var teacher = teachers.First();
            teacher.Teach.Add(discipline);
            await context.SaveChangesAsync();
            if (ctx is null) { await context.DisposeAsync(); }
            return Error.Bottom;
        }
    }

    public class Educate
    {
        public static async Task<Either<Error, bool>> Inspect(
            string           teacherName,
            string           teacherSurname,
            string           studentName,
            string           studentSurname,
            string           disciplineName,
            RelationalDbContext? ctx = null
        )
        {
            var context = ctx ?? RelationalDbContext.New();
            var disciplines = context.Disciplines
                .Where(d => d.Title == disciplineName)
                .Include(d => d.Study)
                .Include(d => d.Teach)
                .HeadOrNone();
            if (disciplines.IsNone) { return Errors.NotFound(); }
            var discipline = disciplines.First();
            var result = discipline.Study.Any(s => s.Name == studentName && s.Surname == studentSurname) &&
                discipline.Teach.Any(t => t.Name == teacherName && t.Surname == teacherSurname);
            await context.SaveChangesAsync();
            if (ctx is null) { await context.DisposeAsync(); }
            return result;
        }
    }
}