using LanguageExt.Common;

namespace Core
{
    public class Errors
    {
        public const int NotFoundCode                      = 404;
        public const int CannotCreateEntityCode            = 901;
        public const int UnacceptableValueForAttributeCode = 902;

        public static Error NotFound(string msg = "") =>
            Error.New(NotFoundCode, $"NotFound: {msg}");

        public static Error CannotCreateEntity(string msg = "") =>
            Error.New(CannotCreateEntityCode, $"CannotCreateEntity: {msg}");

        public static Error UnacceptableValueForAttribute(string msg = "") =>
            Error.New(UnacceptableValueForAttributeCode, $"UnacceptableValueForAttribute: {msg}");
    }
}