using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Relational;
using LanguageExt.Common;
using TinyCsvParser;
using TinyCsvParser.Mapping;

namespace Core.Data
{
    public static class Operations
    {
        public static async Task Import(string path)
        {
            var studentData = Parse(Path.Join(path, "Students.csv"), new StudentData.Mapping());
            var teacherData = Parse(Path.Join(path, "Teachers.csv"), new TeacherData.Mapping());
            var disciplineData = Parse(Path.Join(path, "Disciplines.csv"), new DisciplineData.Mapping());
            foreach (var data in disciplineData)
            {
                var result = await Discipline.Create(data.Name, data.Semester);
                result.IfLeft(e => throw new Exception(e.Message));
            }
            foreach (var data in studentData)
            {
                var result = await Student.Create(data.Name, data.Surname, data.AdmissionYear);
                result.IfLeft(e => throw new Exception(e.Message));
                foreach (var disciplineName in data.Disciplines.Split(";"))
                {
                    var err = await Study.Emit(data.Name, data.Surname, disciplineName);
                    if (!err.Equals(Error.Bottom)) { throw new Exception(err.Message); }
                }
            }
            foreach (var data in teacherData)
            {
                var result = await Teacher.Create(data.Name, data.Surname, data.EmploymentHistory);
                result.IfLeft(e => throw new Exception(e.Message));
                foreach (var disciplineName in data.Disciplines.Split(";"))
                {
                    var err = await Teach.Emit(data.Name, data.Surname, disciplineName);
                    if (!err.Equals(Error.Bottom)) { throw new Exception(err.Message); }
                }
            }
        }

        private static List<T> Parse<T>(string path, CsvMapping<T> mapping)
            where T : class, new()
        {
            var csvParserOptions = new CsvParserOptions(true, ',');
            var csvParser = new CsvParser<T>(csvParserOptions, mapping);
            var records = csvParser.ReadFromFile(path, Encoding.UTF8);
            return records.Select(x => x.Result).ToList();
        }
    }

    public class StudentData
    {
        public string Name          { get; set; }
        public string Surname       { get; set; }
        public int    AdmissionYear { get; set; }
        public string Disciplines   { get; set; }

        internal class Mapping : CsvMapping<StudentData>
        {
            internal Mapping()
            {
                this.MapProperty(0, x => x.Name);
                this.MapProperty(1, x => x.Surname);
                this.MapProperty(2, x => x.AdmissionYear);
                this.MapProperty(3, x => x.Disciplines);
            }
        }
    }

    public class TeacherData
    {
        public string Name              { get; set; }
        public string Surname           { get; set; }
        public string EmploymentHistory { get; set; }
        public string Disciplines       { get; set; }

        internal class Mapping : CsvMapping<TeacherData>
        {
            internal Mapping()
            {
                this.MapProperty(0, x => x.Name);
                this.MapProperty(1, x => x.Surname);
                this.MapProperty(2, x => x.EmploymentHistory);
                this.MapProperty(3, x => x.Disciplines);
            }
        }
    }

    public class DisciplineData
    {
        public string Name     { get; set; }
        public int    Semester { get; set; }

        internal class Mapping : CsvMapping<DisciplineData>
        {
            internal Mapping()
            {
                this.MapProperty(0, x => x.Name);
                this.MapProperty(1, x => x.Semester);
            }
        }
    }
}